package com.iut.app.android.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class MainActivity extends AppCompatActivity {

    EditText etName = null;
    TextInputEditText tieFirstName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etName = findViewById(R.id.et_name);
        tieFirstName = findViewById(R.id.tie_firstName);

        Button btn = findViewById(R.id.btnValidate);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkName();
            }
        });


        Log.e("MainActivity", "onCreate");
        Log.d("IUT", "onCreate DEbug");
    }


    private void checkName() {

        String text = etName.getText().toString();
        if(text.isEmpty()) {
            Log.e("checkName", "no name");
            Toast.makeText(this, "No name", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean checkString() {
        return true;
    }

    private Boolean isFieldEmpty(String value) {
        return true;
    }

    private void submitForm() {
        if (areFieldsFilled()) {
            // Ouvrir l'autre vue
        } else {
            Toast.makeText(this, "Tous les champs ne sont pas remplis", Toast.LENGTH_SHORT).show();
        }
    }

    private Boolean areFieldsFilled() {

        if (etName.getText().toString().isEmpty()) {
            return false;
        }

        if (tieFirstName.getText().toString().isEmpty()) {
            return false;
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("MainActivity", "onResume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.e("MainActivity", "onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e("MainActivity", "onPause");
    }

    @Override
    protected void onDestroy() {

        Log.e("MainActivity", "onDestroy");
        super.onDestroy();
    }
}